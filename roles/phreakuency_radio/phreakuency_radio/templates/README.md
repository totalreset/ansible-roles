# Role to deploy a website with apache2 and TLS support via letsencrypt

This role deploys a website with apache and uses certbot to create TLS
certificates and enables them in the apache vhost

Need to make a dictionary in your var file that looks like:
```
resetup.resetup.icecast:
  name: 
  domain:
```

Replace variables ex
```
grep -Rrl resetup.resetup.icecast | xargs sed -i 's/resetup.resetup.icecast/foo/g' 
```

Then you need to rename folders and file
```
find -type f -name *resetup.resetup.icecast* | xargs rename resetup.resetup.icecast foo *
find -type d -name resetup.resetup.icecast | xargs rename resetup.resetup.icecast foo *
```
Note: you need to run the last command twice as it will need to look into the 
new renamed folders 
