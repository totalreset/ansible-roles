# Role to deploy a website with apache2 and TLS support via letsencrypt

This role deploys a website with apache and uses certbot to create TLS
certificates and enables them in the apache vhost

Need to make a dictionary in your var file that looks like:
```
paginax:
  name: 
  domain:
```

Replace variables ex
```
grep -Rrl paginax | xargs sed -i 's/paginax/foo/g' 
```

Then you need to rename folders and file
```
find -type f -name *paginax* | xargs rename paginax foo *
find -type d -name paginax | xargs rename paginax foo *
```
Note: you need to run the last command twice as it will need to look into the 
new renamed folders 
